package com.spyrosk.expensetracker.ui.filter_date

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.spyrosk.expensetracker.utils.currentDateInMillis
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FilterDateDialogViewModel @Inject constructor(
	private val savedState: SavedStateHandle,
) : ViewModel() {
	
	private val dateKey = "date_key"
	
	private val filterDateEventChannel = Channel<FilterDateEvent>()
	val filterDateEventFlow = filterDateEventChannel.receiveAsFlow()
	
	val titleFromArgs = savedState.get<String>("title_arg")	// This arg is non-nullable
	private val dateFromArgs = savedState.get<Long>("dateAsLong_arg")
	
	var date = savedState.get<Long>(dateKey) ?: dateFromArgs ?: currentDateInMillis()
		set(value) {
			field = value
			savedState.set(dateKey, value)
		}
	
	fun clearClicked() {
		viewModelScope.launch {
			filterDateEventChannel.send(FilterDateEvent.NavigateBackWithResult(dateCleared = true))
		}
	}
	
	fun applyClicked() {
		viewModelScope.launch {
			filterDateEventChannel.send(FilterDateEvent.NavigateBackWithResult(resultDate = date))
		}
	}
	
	// Inner classes
	
	sealed class FilterDateEvent {
		data class NavigateBackWithResult(
			val resultDate: Long? = null,
			val dateCleared: Boolean = false
		) : FilterDateEvent()
	}
	
}
