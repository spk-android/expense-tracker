package com.spyrosk.expensetracker.ui.transactions

import java.math.BigDecimal

data class TransactionsParamCombination(
	val categories: Set<Int>?,
	val amountMin: BigDecimal?,
	val amountMax: BigDecimal?,
	val dateStart: Long?,
	val dateEnd: Long?,
	val orderDescending: Boolean
)
