package com.spyrosk.expensetracker.ui.filters

enum class DateFilterType {
	START_DATE,
	END_DATE
}
