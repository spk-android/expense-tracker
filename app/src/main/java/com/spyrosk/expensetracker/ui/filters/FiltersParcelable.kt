package com.spyrosk.expensetracker.ui.filters

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.math.BigDecimal

@Parcelize
data class FiltersParcelable (
	val selectedCategoryIDs: MutableSet<Int>? = null,
	val amountMin: BigDecimal? = null,
	val amountMax: BigDecimal? = null,
	val dateStart: Long? = null,
	val dateEnd: Long? = null,
	val filtersCleared: Boolean = false
) : Parcelable
