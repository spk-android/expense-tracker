package com.spyrosk.expensetracker.ui.filter_date

import android.app.Dialog
import android.os.Bundle
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.spyrosk.expensetracker.R
import com.spyrosk.expensetracker.databinding.FragmentFilterDateBinding
import com.spyrosk.expensetracker.utils.dateToMillis
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class FilterDateDialogFragment : DialogFragment() {
	
	private val viewModel: FilterDateDialogViewModel by viewModels()
	private lateinit var binding: FragmentFilterDateBinding
	
	override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
		binding = FragmentFilterDateBinding.inflate(layoutInflater)
		
		return AlertDialog.Builder(ContextThemeWrapper(requireContext(), R.style.CustomDialogStyle))
			.setTitle(viewModel.titleFromArgs)
			.setView(binding.root)
			.setNegativeButton(getString(R.string.cancel_text), null)
			.setPositiveButton(getString(R.string.apply_text)) { _, _ -> viewModel.applyClicked() }
			.setNeutralButton(getString(R.string.clear_text)) { _, _ -> viewModel.clearClicked() }
			.create()
	}
	
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
		binding.calDate.date = viewModel.date
		
		binding.calDate.setOnDateChangeListener { _, year, month, day ->
			viewModel.date = dateToMillis(year, month, day)
		}
		
		listenToViewModelEvents()
		
		return binding.root
	}
	
	private fun listenToViewModelEvents() {
		viewLifecycleOwner.lifecycleScope.launchWhenStarted {
			viewModel.filterDateEventFlow.collect { event -> handleEvent(event) }
		}
	}
	
	private fun handleEvent(event: FilterDateDialogViewModel.FilterDateEvent) {
		when(event) {
			is FilterDateDialogViewModel.FilterDateEvent.NavigateBackWithResult -> {
				setFragmentResult(DATE_DIALOG_FRAGMENT_RESULT_REQUEST_KEY, bundleOf(
					DATE_DIALOG_FRAGMENT_RESULT_DATE_KEY to event.resultDate,
					DATE_DIALOG_FRAGMENT_RESULT_DATE_CLEARED_KEY to event.dateCleared))
				findNavController().popBackStack()
			}
		}
	}
	
}

const val DATE_DIALOG_FRAGMENT_RESULT_REQUEST_KEY = "DATE_DIALOG_FRAGMENT_RESULT_REQUEST_KEY"
const val DATE_DIALOG_FRAGMENT_RESULT_DATE_KEY = "DATE_DIALOG_FRAGMENT_RESULT_DATE_KEY"
const val DATE_DIALOG_FRAGMENT_RESULT_DATE_CLEARED_KEY = "DATE_DIALOG_FRAGMENT_RESULT_DATE_CLEARED_KEY"
