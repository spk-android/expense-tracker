package com.spyrosk.expensetracker.ui.transactions

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import com.spyrosk.expensetracker.R
import dagger.hilt.android.AndroidEntryPoint
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.collect
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.leinardi.android.speeddial.SpeedDialActionItem
import com.spyrosk.expensetracker.data.Transaction
import com.spyrosk.expensetracker.data.TransactionWithCategory
import com.spyrosk.expensetracker.databinding.FragmentTransactionsBinding
import com.spyrosk.expensetracker.ui.filters.*
import com.spyrosk.expensetracker.utils.TransactionDetailsFragmentTitleProvider.Companion.getTitleForExisting as getTitleForExisting
import com.spyrosk.expensetracker.utils.TransactionDetailsFragmentTitleProvider.Companion.getTitleForNew as getTitleForNew

@AndroidEntryPoint
class TransactionsFragment : Fragment(R.layout.fragment_transactions) {
	
	private val viewModel: TransactionsViewModel by viewModels()
	private lateinit var binding: FragmentTransactionsBinding
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		
		setHasOptionsMenu(true)
		
		binding = FragmentTransactionsBinding.bind(view)
		
		binding.fabAddTransaction.inflate(R.menu.menu_add_transaction)
		binding.fabAddTransaction.setOnActionSelectedListener(this::onFabActionSelected)
		
		val transactionsAdapter = TransactionsAdapter { t -> onTransactionClicked(t) }
		binding.rcvTransactions.adapter = transactionsAdapter
		binding.rcvTransactions.layoutManager = LinearLayoutManager(requireContext())
		binding.rcvTransactions.setHasFixedSize(true)
		viewModel.transactionsWithCategoriesLiveData.observe(viewLifecycleOwner) { list ->
			onTransactionsChange(transactionsAdapter, list)
		}
		addSwipeFunctionality(binding.rcvTransactions)
		
		setFragmentResultListener(FILTERS_DIALOG_FRAGMENT_RESULT_REQUEST_KEY, processResult())
		
		listenToViewModelEvents()
	}
	
	private fun onTransactionsChange(transactionsAdapter: TransactionsAdapter, list: List<TransactionWithCategory>?, ) {
		transactionsAdapter.submitList(list)
		if(!list.isNullOrEmpty())
			viewModel.transactionsFetched()
		else
			viewModel.transactionsMissing()
	}
	
	private fun processResult() = { _: String, bundle: Bundle ->
		val filters = bundle.get(FILTERS_DIALOG_FRAGMENT_RESULT_KEY_FILTERS) as FiltersParcelable
		viewModel.filterResultsReceived(filters)
	}
	
	override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
		super.onCreateOptionsMenu(menu, inflater)
		
		inflater.inflate(R.menu.menu_fragment_transactions, menu)
		
		menu.findItem(R.id.act_filter).setOnMenuItemClickListener {
			viewModel.onActionFilterSelected()
			true
		}
		
		
		val sortDescOptionItem = menu.findItem(R.id.act_sort_desc)
		val sortAscOptionItem = menu.findItem(R.id.act_sort_asc)
		
		sortDescOptionItem.setOnMenuItemClickListener {
			viewModel.onActionSortDescendingSelected()
			true
		}
		sortAscOptionItem.setOnMenuItemClickListener {
			viewModel.onActionSortAscendingSelected()
			true
		}
	}
	
	private fun addSwipeFunctionality(recyclerView: RecyclerView) {
		val itemSwipeHelper = ItemTouchHelper(
			object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
				
				override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder, ): Boolean {
					return false
				}
				
				override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
					val transaction = (recyclerView.adapter as TransactionsAdapter)
						.currentList[viewHolder.adapterPosition].transaction
					viewModel.onTransactionSwiped(transaction)
				}
				
			})
		
		itemSwipeHelper.attachToRecyclerView(binding.rcvTransactions)
	}
	
	private fun onTransactionClicked(transaction: Transaction) {
		viewModel.onTransactionClicked(transaction)
	}
	
	private fun listenToViewModelEvents() {
		viewLifecycleOwner.lifecycleScope.launchWhenStarted {
			viewModel.transactionsEventFlow.collect { event -> handleEvent(event) }
		}
	}
	
	private fun handleEvent(event: TransactionsViewModel.TransactionsEvent) {
		when(event) {
			is TransactionsViewModel.TransactionsEvent.NavigateToAddTransactionScreen -> {
				val action = TransactionsFragmentDirections.actionGlobalTransactionDetailsFragment(
							event.transactionType.name,
							titleArg = getTitleForNew(requireContext(), event.transactionType))
				findNavController().navigate(action)
			}
			is TransactionsViewModel.TransactionsEvent.NavigateToEditTransactionScreen -> {
				val action = TransactionsFragmentDirections.actionGlobalTransactionDetailsFragment(
							transactionTypeArg = event.transaction.transactionType.name,
							transactionArg = event.transaction,
							titleArg = getTitleForExisting(requireContext(), event.transaction))
				findNavController().navigate(action)
			}
			is TransactionsViewModel.TransactionsEvent.ShowUndoTransactionDeleteMessage -> {
				val title = event.transaction.title
				val message = getString(R.string.transactions_transaction_deleted_msg) + " \"$title\""
				Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG)
					.setAction(getString(R.string.undo_text)) { viewModel.onTransactionDeleteUndone(event.transaction) }
					.show()
			}
			is TransactionsViewModel.TransactionsEvent.NavigateToFilterDialog -> {
				val action = TransactionsFragmentDirections.actionTransactionsFragmentToFiltersDialogFragment(
					filtersArg = viewModel.createFiltersParcelable())
				findNavController().navigate(action)
			}
			is TransactionsViewModel.TransactionsEvent.ChangeNoTransactionsMessageVisibility -> {
				if(event.visible)
					binding.tvNoTransactionsMessage.visibility = View.VISIBLE
				else
					binding.tvNoTransactionsMessage.visibility = View.GONE
			}
		}
	}
	
	private fun onFabActionSelected(item: SpeedDialActionItem): Boolean {
		when(item.id) {
			R.id.act_add_income -> {
				viewModel.onActionAddIncomeClicked()
				binding.fabAddTransaction.close()
				return true
			}
			R.id.act_add_expense -> {
				viewModel.onActionAddExpenseClicked()
				binding.fabAddTransaction.close()
				return true
			}
			else -> return false
		}
	}
	
}
