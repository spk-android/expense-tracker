package com.spyrosk.expensetracker.data

import com.spyrosk.expensetracker.R
import com.spyrosk.expensetracker.utils.ResourceProvider
import javax.inject.Inject

class CategorySeeder @Inject constructor(private val p: ResourceProvider) {
	
	fun getDefaultCategories(): ArrayList<Category> {
		return arrayListOf(
			Category(p.getString(R.string.category_name_undefined_expense),		p.getColor(R.color.red_grey),		TransactionType.EXPENSE, id = 1),
			Category(p.getString(R.string.category_name_undefined_income),		p.getColor(R.color.blue_grey),		TransactionType.INCOME, id = 2),
			
			Category(p.getString(R.string.category_name_grocery),		p.getColor(R.color.green),		TransactionType.EXPENSE),
			Category(p.getString(R.string.category_name_transportation),p.getColor(R.color.teal),		TransactionType.EXPENSE),
			Category(p.getString(R.string.category_name_restaurants),	p.getColor(R.color.orange),		TransactionType.EXPENSE),
			Category(p.getString(R.string.category_name_entertainment),	p.getColor(R.color.dark_pink),	TransactionType.EXPENSE),
			Category(p.getString(R.string.category_name_shopping),		p.getColor(R.color.purple),		TransactionType.EXPENSE),
			Category(p.getString(R.string.category_name_health),		p.getColor(R.color.red),		TransactionType.EXPENSE),
			
			Category(p.getString(R.string.category_name_salary),		p.getColor(R.color.blue),		TransactionType.INCOME),
			Category(p.getString(R.string.category_name_allowance),		p.getColor(R.color.light_blue),	TransactionType.INCOME)
		)
	}
	
}
