package com.spyrosk.expensetracker.data

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface CategoryDao {
	
	@Query(value = "select * from categories")
	fun getAllCategories(): Flow<List<Category>>
	
	@Query(value = "select * from categories where applicableTransactionType = :transactionTypeName")
	fun getAllForType(transactionTypeName: String): Flow<List<Category>>
	
	fun getAllForType(transactionType: TransactionType) =
		getAllForType(transactionType.name)
	
	@Insert(onConflict = OnConflictStrategy.REPLACE)
	suspend fun insert(category: Category)
	
	@Update
	suspend fun update(category: Category)
	
	@Delete
	suspend fun delete(category: Category)
	
}
