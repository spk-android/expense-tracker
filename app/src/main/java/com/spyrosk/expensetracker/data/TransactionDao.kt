package com.spyrosk.expensetracker.data

import androidx.room.*
import kotlinx.coroutines.flow.Flow
import java.math.BigDecimal

@Dao
interface TransactionDao {
	
	// Queries
	
	@Query(value = "select * from transactions order by dateTimestamp desc")
	fun getAllTransactions(): Flow<List<Transaction>>
	
	@Query(value = "select * from transactions order by dateTimestamp desc limit :resultsLimit")
	fun getTransactionsLimited(resultsLimit: Int): Flow<List<Transaction>>
	
	fun getTransactions(resultsLimit: Int = -1): Flow<List<Transaction>> =
		if(resultsLimit == -1)
			getAllTransactions()
		else
			getTransactionsLimited(resultsLimit)
	
	fun getTransactionsByCategory(category: Category): Flow<List<Transaction>> =
		getTransactionsByCategoryId(category.id)
	
	@Query(value = "select * from transactions where category_id = :categoryId")
	fun getTransactionsByCategoryId(categoryId: Int): Flow<List<Transaction>>
	
	// Insert-Update-Delete
	
	@Insert(onConflict = OnConflictStrategy.REPLACE)
	suspend fun insert(transaction: Transaction)
	
	@Update
	suspend fun update(transaction: Transaction)
	
	@Delete
	suspend fun delete(transaction: Transaction)
	
	// Other
	
	@Query(value = "select sum(amount_in_cents) from transactions where transactionType = :transactionTypeName")
	fun getSumOf(transactionTypeName: String): Flow<BigDecimal>
	
	@Query(value = "select sum(amount_in_cents) from transactions")
	fun getSumOfAll(): Flow<BigDecimal>
	
	fun getSum(transactionType: TransactionType? = null): Flow<BigDecimal> =
		when(transactionType) {
			TransactionType.EXPENSE, TransactionType.INCOME -> getSumOf(transactionType.name)
			null -> getSumOfAll()
		}
	
}
