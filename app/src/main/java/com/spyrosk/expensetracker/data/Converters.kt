package com.spyrosk.expensetracker.data

import androidx.room.TypeConverter
import java.math.BigDecimal

class Converters {
	
	@TypeConverter
	fun transactionTypeToString(transactionType: TransactionType) = transactionType.name
	
	@TypeConverter
	fun transactionTypeFromString(string: String) = TransactionType.valueOf(string)
	
	
	@TypeConverter
	fun decimalCurrencyToLongCents(bigDecimal: BigDecimal) = bigDecimal.multiply(BigDecimal("100")).toLong()
	
	@TypeConverter
	fun decimalCurrencyFromLongCents(long: Long) = BigDecimal(long).divide(BigDecimal("100"))!!
	
}
