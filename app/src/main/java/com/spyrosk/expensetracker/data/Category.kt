package com.spyrosk.expensetracker.data

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(tableName = "categories")
@Parcelize
data class Category(
	val name: String,
	val color: Int,
	val applicableTransactionType: TransactionType,
	@PrimaryKey(autoGenerate = true) val id: Int = 0
) : Parcelable
