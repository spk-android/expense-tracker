package com.spyrosk.expensetracker.utils

import android.content.Context
import com.spyrosk.expensetracker.R
import com.spyrosk.expensetracker.data.Transaction
import com.spyrosk.expensetracker.data.TransactionType

class TransactionDetailsFragmentTitleProvider {
	
	companion object Companion {
		
		fun getTitleForExisting(context: Context, transaction: Transaction): String {
			return when(transaction.transactionType) {
				TransactionType.INCOME -> context.getString(R.string.transaction_details_fragment_title_edit_income)
				TransactionType.EXPENSE -> context.getString(R.string.transaction_details_fragment_title_edit_expense)
			}
		}
		
		fun getTitleForNew(context: Context, transactionType: TransactionType): String {
			return when(transactionType) {
				TransactionType.INCOME -> context.getString(R.string.transaction_details_fragment_title_new_income)
				TransactionType.EXPENSE -> context.getString(R.string.transaction_details_fragment_title_new_expense)
			}
		}
		
	}
	
}