package com.spyrosk.expensetracker.utils

import android.content.Context
import android.os.Build

fun getColorOpt(context: Context, colorId: Int): Int {
	if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
		return context.resources.getColor(colorId, context.theme)
	
	@Suppress("DEPRECATION")
	return context.resources.getColor(colorId)
}
