package com.spyrosk.expensetracker.utils

import java.math.BigDecimal
import java.text.NumberFormat
import java.util.*

class CurrencyFormatter {
	
	companion object Companion {
		
		fun format(
			amount: BigDecimal, autoHideDecimals: Boolean = true,
			isNegative: Boolean = false, usePlusSign: Boolean = true, useSymbol: Boolean = true,
		): String {
			val format: NumberFormat = NumberFormat.getInstance()
			format.maximumFractionDigits = 2
			format.minimumFractionDigits = 2
			var numberString = format.format(amount)
			
			numberString = numberString.trim()
			
			if(autoHideDecimals && numberString.endsWith(".00"))
				numberString = numberString.substring(0, numberString.length - 3)
			
			val sign = if(!isNegative && usePlusSign) "+ " else if(isNegative) "− " else ""
			
			val symbol = if(useSymbol) " " + Currency.getInstance(Locale.getDefault()).symbol else ""
			
			return "$sign$numberString$symbol"
		}
		
		fun formatPlain(amount: BigDecimal) =
			format(amount,
				isNegative = false,
				usePlusSign = false,
				autoHideDecimals = false,
				useSymbol = false)
		
	}
	
}
